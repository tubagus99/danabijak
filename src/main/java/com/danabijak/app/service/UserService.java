package com.danabijak.app.service;

import com.danabijak.app.model.User;
import com.danabijak.app.security.UserPrincipal;
import org.springframework.http.ResponseEntity;

public interface UserService {

    public User getCurrentUser(Long id);

    public ResponseEntity<?> balance(UserPrincipal currentUser);

}
