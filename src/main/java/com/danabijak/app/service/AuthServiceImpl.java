package com.danabijak.app.service;

import com.danabijak.app.constants.HttpStatusCode;
import com.danabijak.app.exception.AppException;
import com.danabijak.app.model.Account;
import com.danabijak.app.model.Role;
import com.danabijak.app.model.Transaction;
import com.danabijak.app.model.User;
import com.danabijak.app.model.enums.RoleName;
import com.danabijak.app.model.enums.TrxSide;
import com.danabijak.app.model.enums.TrxType;
import com.danabijak.app.payload.request.SignInRequest;
import com.danabijak.app.payload.request.SignUpRequest;
import com.danabijak.app.payload.response.ApiResponse;
import com.danabijak.app.payload.response.JwtAuthResponse;
import com.danabijak.app.security.JwtTokenProvider;
import java.net.URI;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.danabijak.app.repository.AccountRepository;
import com.danabijak.app.repository.RoleRepository;
import com.danabijak.app.repository.TransactionRepository;
import com.danabijak.app.repository.UserRepository;

@Service("AuthService")
public class AuthServiceImpl implements AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private TransactionRepository transactionRep;

    @Override
    public ResponseEntity<?> signIn(SignInRequest body) {
        Optional<User> optUser = userRepository.findByEmail(body.getEmail());
        if (!optUser.isPresent()) {
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email/Username or Password not valid!", null),
                    HttpStatus.BAD_REQUEST);
        }

        User user = optUser.get();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        body.getEmail(),
                        body.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        JwtAuthResponse res = new JwtAuthResponse(jwt);

        return ResponseEntity.ok(new ApiResponse(true, null, "token generated", res));
    }

    @Override
    public ResponseEntity<?> signUp(SignUpRequest body) {
        if (userRepository.existsByEmail(body.getEmail())) {
            logger.info("signup : email " + body.getEmail() + " already exist");
            return new ResponseEntity(new ApiResponse(false, HttpStatusCode.BAD_REQUEST.asText(), "Email Address already in use!", null),
                    HttpStatus.BAD_REQUEST);
        }

        String[] arrStr = body.getEmail().split("@");
        String userName = arrStr[0];

        User user = new User(userName,
                body.getEmail(), body.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));
        user.setRoles(Collections.singleton(userRole));
        User userSaved = userRepository.save(user);

        Account account = new Account();
        account.setCurrency("USD");
        account.setBalance(100d);
        account.setUser(userSaved);
        Account accountSaved = accountRepository.save(account);

        Transaction trx = new Transaction();
        trx.setAccount(accountSaved);
        trx.setAmount(100d);
        trx.setRemark("initiate");
        trx.setSide(TrxSide.CREDIT);
        trx.setType(TrxType.DEPOSIT);
        trx.setTrxDate(new Date());
        transactionRep.save(trx);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(user.getUserName()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, null, "User registered successfully. Please login", null));
    }

}
