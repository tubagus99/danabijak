package com.danabijak.app.service;

import com.danabijak.app.payload.request.DepositRequest;
import com.danabijak.app.payload.request.WithdrawalRequest;
import com.danabijak.app.security.UserPrincipal;
import org.springframework.http.ResponseEntity;

public interface TransactionService {

    public ResponseEntity<?> deposit(DepositRequest body, UserPrincipal currentUser);

    public ResponseEntity<?> withdrawal(WithdrawalRequest body, UserPrincipal currentUser);
}
