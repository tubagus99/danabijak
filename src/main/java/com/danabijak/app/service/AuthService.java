package com.danabijak.app.service;

import com.danabijak.app.payload.request.SignInRequest;
import com.danabijak.app.payload.request.SignUpRequest;
import org.springframework.http.ResponseEntity;

public interface AuthService {

    public ResponseEntity<?> signIn(SignInRequest body);

    public ResponseEntity<?> signUp(SignUpRequest body);

}
