package com.danabijak.app.repository;

import com.danabijak.app.model.Transaction;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    public List<Transaction> findAllByAccountId(Long id);
}
